package com.treebo.test;

import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    private RelativeLayout mContainer;
    private WebView mWebView;
    private WebView mNewWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContainer = (RelativeLayout) findViewById(R.id.container);
        mWebView = (WebView) findViewById(R.id.webview);

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                mNewWebView = new WebView(MainActivity.this);
                mNewWebView.setLayoutParams(new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.MATCH_PARENT
                ));

                WebSettings webSettings = mNewWebView.getSettings();
                webSettings.setJavaScriptEnabled(true);
                webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

                mNewWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        view.clearHistory();
                    }
                });
                mContainer.addView(mNewWebView);

                ((WebView.WebViewTransport) resultMsg.obj).setWebView(mNewWebView);
                resultMsg.sendToTarget();

                return true;
            }
        });
        mWebView.setWebViewClient(new WebViewClient());

        mWebView.loadData("<a href=\"https://example.com\" target=\"_blank\" rel=\"noreferrer\">open new window</a>",
                "text/html", "utf-8");
    }

    @Override
    public void onBackPressed() {
        if (mNewWebView != null) {
            if (mNewWebView.canGoBack()) {
                mNewWebView.goBack();
            } else {
                mNewWebView.destroy();
                mContainer.removeViewAt(1);
                mNewWebView = null;
            }
        } else if (mWebView != null && mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
